package com.adisalagic.dndcombatroller;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.JsonWriter;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.StringWriter;

public class MainActivityP extends AppCompatActivity {
    String sCharacterName;
    Button submit;
    EditText init, life;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_p);
        sCharacterName = getIntent().getStringExtra("Nick");
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        TextView characterName = findViewById(R.id.character_name);
        characterName.setText(sCharacterName);
        submit = findViewById(R.id.submit_stat);
        init = findViewById(R.id.initiative);
        life = findViewById(R.id.LifePoints);
        life.setVisibility(EditText.INVISIBLE);
    }
    /**
     * Submit button, that should next connect player to server
     * It has only a modifier to check.
     */
    public void onSubmitClick(View view){
        if (init.getText().length() != 0) {
            //gmWaiting.setVisibility(TextView.VISIBLE);
            submit.setEnabled(false);
        }else {
            Toast.makeText(this, "Введите модификатор!", Toast.LENGTH_SHORT).show();
        }
        //Connect to the server
    }
    /**
     * This button helps create new stat for character
     * It almost works. But dialog isn't work well! :c
     */
    public void onStatsClick(View view){
        Intent intent = new Intent();
        intent.setClass(this, custom_stats.class);
        startActivity(intent);
    }
    /**
     * This method is for writing JSON, but I don't understand
     * how to write JSONs
     */
    public void writeJSON(){
        StringWriter sw = new StringWriter();
        JsonWriter jw = new JsonWriter(sw);
    }
}
