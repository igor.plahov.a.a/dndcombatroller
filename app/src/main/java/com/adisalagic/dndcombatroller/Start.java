package com.adisalagic.dndcombatroller;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebSettings;
import android.webkit.WebView;

import java.util.Timer;

public class Start extends AppCompatActivity {
    WebView webView;
    private String url = "http://192.168.43.196:8000/";
    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        webView = findViewById(R.id.web);
        webView.setWebViewClient(new web());
        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.loadUrl(url);
        webView.scrollTo(0,1);

    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.web_view);
    }

    @Override
    public void onBackPressed() {
        if (webView.canGoBack()){
            webView.goBack();
        }else {
            super.onBackPressed();
        }

    }
    /*
     * -Omg, Kira, are you alright?
     * -I guess so..., - he said, - but I feel we ran out of time
     * -Yes, we have.
     * -It's ok, we got them next time
     */
}
