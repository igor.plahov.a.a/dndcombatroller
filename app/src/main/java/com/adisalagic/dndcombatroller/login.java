package com.adisalagic.dndcombatroller;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class login extends AppCompatActivity {
    TextView characterName;
    Button player, master;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        player = findViewById(R.id.b_player);
        master = findViewById(R.id.b_master);
        player.setEnabled(false);
        master.setEnabled(false);
        characterName = findViewById(R.id.nickname);
        //Creating listener
        characterName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() != 0){
                    player.setEnabled(true);
                    master.setEnabled(true);
                }else{
                    player.setEnabled(false);
                    master.setEnabled(false);
                }
            }
            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public void onPlayerClick(View view){
        characterName = findViewById(R.id.nickname);
        Intent intent = new Intent();
        intent.setClass(this, MainActivityP.class);
        if (!characterName.getText().toString().isEmpty()) {
            intent.putExtra("Nick", characterName.getText().toString());
            startActivity(intent);
        }
    }

    public void onMasterClick(View view){
        characterName = findViewById(R.id.nickname);
        Intent intent = new Intent();
        intent.setClass(this, MainActivityM.class);
        intent.putExtra("Nick", characterName.getText().toString());
        startActivity(intent);
    }
}
