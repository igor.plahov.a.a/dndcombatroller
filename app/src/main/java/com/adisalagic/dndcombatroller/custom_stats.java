package com.adisalagic.dndcombatroller;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;

public class custom_stats extends AppCompatActivity {
    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;
    private static String name;
    private static int min, max;
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        FloatingActionButton fab = findViewById(R.id.fab);

    }


    public void ShowCreateWindow(View view){
        CustomDialog cd = new CustomDialog(this);
        cd.custom_stats = this;
        cd.show();
    }

    public void addFragment(String name, int min, int max){
        fragmentTransaction = fragmentManager.beginTransaction();
        fragment_statistic fs = new fragment_statistic();
        fs.setNewFragment(name, min, max);
        LinearLayout linearLayout = findViewById(R.id.linlay);
        linearLayout.setId(R.id.linlay);
        fragmentTransaction.add(linearLayout.getId(), fs);
        fragmentTransaction.commit();
    }
}
