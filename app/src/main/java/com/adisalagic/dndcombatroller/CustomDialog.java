package com.adisalagic.dndcombatroller;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

public class CustomDialog extends Dialog implements View.OnClickListener{
    Button submit, cancel;
    EditText name;
    EditText min, max;

    @Override
    protected void onStart() {
        super.onStart();
        Window window = getWindow();
    }

    CustomDialog(@NonNull Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_new_statistic);
        submit = findViewById(R.id.submit_stat);
        cancel = findViewById(R.id.cancel_stat);
        name = findViewById(R.id.nameInCreate);
        min = findViewById(R.id.minimum);
        max = findViewById(R.id.maximum);
        submit.setOnClickListener(this);
        cancel.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.submit_stat:
                custom_stats.addFragment("",0,0);
                this.dismiss();
            case R.id.cancel_stat:
                this.dismiss();
        }
    }
    //Help me, plz
    custom_stats custom_stats;
}
