package com.adisalagic.dndcombatroller;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

public class fragment_statistic extends Fragment {
    EditText value;
    TextView tvName;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_statistic, container, false);
    }

    /**
     * This method is for simple set up fragment
     * @param name  Name of stat
     * @param max  Max number of stat
     * @param min  Min number of stat
     */
    protected void setNewFragment(String name, int min, int max){
        if (min > max){
            int Temp;
            Temp = max;
            max = min;
            min = Temp;
        }
        value.findViewById(R.id.custom_name);
        value.setHint(name);
        tvName.findViewById(R.id.nameofstat);
        tvName.setText(name);
        final int finalMax = max;
        final int finalMin = min;
        value.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (Integer.valueOf(s.toString()) > finalMax){
                    value.setText(finalMax);
                }
                if (Integer.valueOf(s.toString()) < finalMin){
                    value.setText(finalMin);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }
}
